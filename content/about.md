---
title: About
---
![Enock Seth Nyamador](/images/enock-seth.jpg)

Hallo! Thanks for your interest.
I'm Enock Seth Nyamador, Free and Open source advocate and trainer. I speak FOSS4G, [FLOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software), Open Data; I contribute to projects such as: [WikiData](https://wikidata.org/wiki/User:Enock4seth), [OpenStreetMap](http://www.openstreetmap.org/user/Enock4seth/), <a href="https://en.wikipedia.org/wiki/User:Enock4seth">Wikipedia</a>. I love travelling, photography, cooking, and dancing ;).

I am very passionate about Free and Open Source Software, maps, mentoring and always looking forward to learning and sharing. I believe in communities of practice.

- [/blog](/posts)
- [/say hello](mailto:open@enockseth.co)
