---
title:  "Volunteer Sign Up - Ghana Volunteer Program (GVP)"
date:   2015-06-01 07:54:34
tag:   
- volunteer 
- Ghana 
- gvp
image: "/images/GVP-logo.png"
---


![GVP Logo](/images/GVP-logo.png)

The [GhanaThink Foundation's](http://ghanathink.org) **[Ghana Volunteer Program](http://www.ghanathink.org/content/ghana-volunteer-program-profile)** is a hub for volunteer activities in Ghana. It was born out of the success of the first National Volunteer Day.

GVP has been able to organize four (4) successful National Volunteer Day ([#NVDay](https://twitter.com/search?q=%23NVDay&vertical=default)) across Ghana. NVDay takes place from **21 - 23 September** of every year.

Currently GVP is creating a database of volunteers across Ghana. **What is the purpose of this database?** This is to gather information on interested volunteers for volunteer opportunities across Ghana through out the year. It will also help GVP place you in the right volunteer activity or one that meets your interest, skill and passion.

You can sign up right here within the form below OR Visit [Volunteer Sign Up](https://docs.google.com/forms/d/1PGfg9-PIvXoJBGXvKuXYXQ31cvcU_M1oC3km0aRrSCs/viewform).

<iframe src="https://docs.google.com/forms/d/1PGfg9-PIvXoJBGXvKuXYXQ31cvcU_M1oC3km0aRrSCs/viewform?embedded=true#start=embed" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>

Also follow GVP on Twitter: [@VolunteerinGH](http://twitter,com/volunteeringh)
 and the hashtag [#volunteeringh](https://twitter.com/search?q=%23volunteeringh&src=typd&vertical=default) for updates. Also on Facebook: [Ghana Volunteer Program](https://www.facebook.com/GhanaVolunteerProgram).

Thanks, Merci, Danke, Akpe!
