---
title:  "Welcome to my new blog"
date:   2015-05-12 01:58:11
category: blog
tag: ghost
---

![Ghost logo](https://upload.wikimedia.org/wikipedia/commons/f/fa/Ghost-Logo.svg)

Hello and welcome to my new home on the Internet. This blog is powered by [Ghost](https://ghost.org/) and on [Github](http://github.com/) static pages.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">For 7 days now I&#39;ve been trying to make &#39;npm start&#39; work on Linux Mint 17.1 for <a href="https://twitter.com/hashtag/Ghost?src=hash">#Ghost</a> but no way. :( <a href="https://twitter.com/hashtag/nodejs?src=hash">#nodejs</a> is installed and running well.</p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/597134408945115136">May 9, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

After days of web searches, installs - uninstalls, getting in to dependency hell and others, I was able to successfully set up Ghost on my local machine running *Linux Mint 17.1 'Rebecca'*.

Since this is my first post here I wouldn't say much. :)

I used to blog at [enockseth.blogspot.com](http://enockseth.blogspot.com) will keep it for references and all updates will reside here. 

In my next post I'll tell you some reasons why I decided to use Ghost.

Stay tunned for more updates from me here. 
Thanks for visiting.
Cheers!
