---
title: "Promoting Volunteerism in Ghana #NVDay18"
date: '2018-09-27 07:00:00 +0000'
tag:
- Ghana
- NVDay
- Volunteer
- Africa
image: "/images/tema-general-hospital.jpg"
---

National Volunteer Day (NVDay) taking place during Founders Day and around September 21 since 2013 to promote and encourage volunteerism among Ghanaians. This year NVDay was once again successful with [53 activities](http://volunteeringh.org) registered and [many other activities](https://twitter.com/hashtag/nvday18?vertical=default&src=hash).

![NVDay 18 Tema Maame](/images/nvday18-tema.jpg)
Personally volunteering has taught and helped me in several ways I never imagined. All I was happy with is making impact and seeing my works benefit and put smiles into peoples hearts. Preceding NVDay 18; I was privileged to talk about volunteerism and the Ghana Volunteer Program with Ato on Kwese TV.
<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">Now: Discussing National Volunteer Day &amp; <a href="https://twitter.com/hashtag/NvDay18?src=hash&amp;ref_src=twsrc%5Etfw">#NvDay18</a> with our guests; <a href="https://twitter.com/Enock4seth?ref_src=twsrc%5Etfw">@Enock4seth</a> &amp; <a href="https://twitter.com/Abocco?ref_src=twsrc%5Etfw">@Abocco</a> on <a href="https://twitter.com/hashtag/KweseHeadStart?src=hash&amp;ref_src=twsrc%5Etfw">#KweseHeadStart</a> <a href="https://t.co/8ePtFDYkAE">pic.twitter.com/8ePtFDYkAE</a></p>&mdash; Kwesé Ghana (@Kwese_GH) <a href="https://twitter.com/Kwese_GH/status/1042676578235371520?ref_src=twsrc%5Etfw">20 septembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


This year I joined Galaxy Fun Club with colleagues from BarCamp Tema for a clean up exercise at Tema General Hospital. It was work and happiness. Meeting new, lovely and old friends. 
<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">Clean up exercise at Tema General Hospital. <a href="https://twitter.com/hashtag/NVDay18?src=hash&amp;ref_src=twsrc%5Etfw">#NVDay18</a> <a href="https://twitter.com/hashtag/Tema00?src=hash&amp;ref_src=twsrc%5Etfw">#Tema00</a> <a href="https://t.co/RpvGFyad3K">pic.twitter.com/RpvGFyad3K</a></p>&mdash; BenjaminAdadevoh (@AdOnAfrica) <a href="https://twitter.com/AdOnAfrica/status/1043050101365776386?ref_src=twsrc%5Etfw">21 septembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

![NVDay 18 Tema Maame](/images/nvday18-tema-maame.jpg)
Volunteering is very important in community and national building. Volunteers make the world a better place. Do something in your own little way to make impact. There are several ways to volunteer from teaching a skill, metoring, contributing to an open source project, clean up execises, blood donations and many more. It was such a wonderful day.
<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">On September 21 - <a href="https://twitter.com/hashtag/FoundersDay?src=hash&amp;ref_src=twsrc%5Etfw">#FoundersDay</a> &amp; National Volunteer Day - I donated blood in <a href="https://twitter.com/hashtag/Accra?src=hash&amp;ref_src=twsrc%5Etfw">#Accra</a>, <a href="https://twitter.com/hashtag/Ghana?src=hash&amp;ref_src=twsrc%5Etfw">#Ghana</a> as part of <a href="https://twitter.com/EventsGHplus?ref_src=twsrc%5Etfw">@EventsGHplus</a>&#39; <a href="https://twitter.com/hashtag/NVDay18?src=hash&amp;ref_src=twsrc%5Etfw">#NVDay18</a> activity &amp; also celebrating <a href="https://twitter.com/hashtag/BarcampGhanaAt10?src=hash&amp;ref_src=twsrc%5Etfw">#BarcampGhanaAt10</a>. <br> This was my 4th in the last 5 years. It&#39;s not easy to do, but let&#39;s <a href="https://twitter.com/hashtag/giveblood?src=hash&amp;ref_src=twsrc%5Etfw">#giveblood</a>.  <a href="https://t.co/xtssImfy7o">pic.twitter.com/xtssImfy7o</a></p>&mdash; Ato Ulzen-Appiah #bcbolga (@Abocco) <a href="https://twitter.com/Abocco/status/1043461762421600257?ref_src=twsrc%5Etfw">22 septembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

[Sign up](https://docs.google.com/forms/d/e/1FAIpQLSfmKpw_Ui-aVnlui5cytx1soNjpoSxPT9rWCZQWzGo1PRb1NQ/viewform?fbzx=-8743635608616909044) to Ghana Volunteer Program volunteer database where we match and connect volunteers to volunteering opportunities across Ghana.
Find us on:
* [Facebook](https://www.facebook.com/GhanaVolunteerProgram/) 
* [Facebook Group](https://www.facebook.com/groups/volunteeringh/)
* [Twitter](https://www.twitter.com/volnteeringh)