---
title: Call for SOTMAfrica 2019 Logo Design
date: '2019-01-02 13:20:00 +0000'  
tag:
- sotmafrica
- openstreetmap
- contest
image: "/images/sotmafrica0.jpg"
description: Design State of the Map Africa 2019 logo
---

[State of the Map Africa 2019](http://2019.stateofthemap.africa/) is taking place in Grand-Bassam, Ivory Coast from 22nd to 24th November 2019. Call for logos is now OPEN. This is an opportunity for 1000s to appreciate your hand works as a designer :)

![Africa](/images/sotmafrica0.jpg)

## Things to note:
* Format: PNG, SVG, PDF
* Deadline: Before 23:59 GMT on January 31, 2019.

## The design should communicate
* The Theme: **"Transforming lives through mapping"**
* The Africa Experience
* Fun
* Cool
* Openness
* A sense of community
* The potential of OpenStreetMap
* A sense of learning and education
* Intelligence
* Trustworthiness
* Global movement

## The design should not communicate

* Corporate / big business (as the sole element)
* Complexity
* Expensive
* For geeks only
* Closed community

More information about the contest can be found on this [OSM Wikipage](https://wiki.openstreetmap.org/wiki/State_of_the_Map_Africa_2019/Logo_Contest)

Take a look at State of the Map 2018, Italy [logo contest entries](https://commons.wikimedia.org/wiki/Category:State_of_the_Map_2018_logo_contest)