---
title:  "HOT Activation Workshop, Dar Es Salaam"
date:   2015-10-20 19:35:15
tag:   
- openstreetmap 
- conference
image:	/images/hotosmlogo.png
---

![](/images/hotosmlogo.png)

I was priviledge to be part of the first [Humanitarian OpenStreetMap Team (HOT)](https://www.hotosm.org/updates/2015-09-08_activation_workshop_at_africa_open_data_conference) workshop that took place in Dar Es Salaam as part of the [Africa Open Data Conference (AODC)](https://www.africaopendata.org/). 

We all know about Google Maps; right? How many have heard of OpenStreetMap? Well OpenStreetMap is a free map of the world that anyone can contribute to; just like Wikipedia of maps.

The Humanitarian OpenStreetMap Team (HOT), an NGO that works to train, coordinate, and organize mapping on OpenStreetMap for humanitarian, disaster response, and economic development, has mobilized volunteers from around the world to help map since the Haiti earthquake in 2010.

The workshop was about what goes into HOT activation. What activation is all about and making it formal. Activation have always been the adhoc type.

This workshop only did not let us learn more about activation but also provided the avenue to meet faces behind some usernames that work tirelessly to create a free, detailed and quality map of the world. I was very glad to meet Russell. 

You know one thing I can't forget is Russell's mouse clicks during my first HOT Mapternoons on Mumble meeting him, I couldn't hide it. 

Not forgetting Bazo, Amadou, Jerioke, Mharia, Samuel, Claire, Triphonia, Justin, Mark, American Red Cross, Ramani Huaria project members; Geoffery et all. You know the list goes on. It was awesome meeting you all.

See you all soon.