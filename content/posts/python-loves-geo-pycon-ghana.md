---
title: Talking Python loves Geo at Pycon Ghana
date: '2018-08-13 06:33:33 +0000'
tag:
- pycon
- ghana
- python
image: "/images/pycon_ghana.png"
---

Great pleasure was it for me talking about how Python loves Geo during PyCon Ghana at the University of Ghana.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">It was great talking &quot;Python and GIS&quot; today <a href="https://twitter.com/PyconGhana?ref_src=twsrc%5Etfw">@PyconGhana</a> featuring <a href="https://twitter.com/qgis?ref_src=twsrc%5Etfw">@QGIS</a> <a href="https://twitter.com/GeoNode?ref_src=twsrc%5Etfw">@GeoNode</a> <a href="https://twitter.com/OSGeo?ref_src=twsrc%5Etfw">@OSGeo</a>  <a href="https://twitter.com/openstreetmap?ref_src=twsrc%5Etfw">@openstreetmap</a>; <a href="https://twitter.com/OSMGhana?ref_src=twsrc%5Etfw">@OSMGhana</a> &amp; <a href="https://twitter.com/youthmappers?ref_src=twsrc%5Etfw">@youthmappers</a>. FOSS Communities &amp; Networking is very important.  <a href="https://twitter.com/hashtag/NaturalEarthData?src=hash&amp;ref_src=twsrc%5Etfw">#NaturalEarthData</a> <a href="https://twitter.com/hashtag/GDAL?src=hash&amp;ref_src=twsrc%5Etfw">#GDAL</a> <a href="https://twitter.com/hashtag/PyConGhana?src=hash&amp;ref_src=twsrc%5Etfw">#PyConGhana</a> <a href="https://twitter.com/hashtag/GISTribe?src=hash&amp;ref_src=twsrc%5Etfw">#GISTribe</a> <a href="https://twitter.com/hashtag/FOSS4G?src=hash&amp;ref_src=twsrc%5Etfw">#FOSS4G</a> cc <a href="https://twitter.com/lusdavo?ref_src=twsrc%5Etfw">@lusdavo</a> <a href="https://twitter.com/wgeary?ref_src=twsrc%5Etfw">@wgeary</a> <a href="https://t.co/FuVvu3soWn">pic.twitter.com/FuVvu3soWn</a></p>&mdash; Enock Seth Nyamador #NVDay18 (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/1028404555346862086?ref_src=twsrc%5Etfw">11 août 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

My talk was basically about an introduction to Geographic Information Systems (GIS), some Python libraries and it's uses in the geospatial world

Some of software, tools and projects I highlighted are:
* [GeoNode](http://geonode.org/)
* [QGIS](https://qgis.org)
* Django & GeoDjango
* [OpenStreetMap](https://osm.org)
* [OSGeo](https://osgeo.org)
* [AccraMobile](http://junglebus.io/accra)

.... and others.

My presentation below:

<iframe src="//www.slideshare.net/slideshow/embed_code/key/GEZixGTX7llK4K" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/Enock4seth/python-loves-gis-pycon-ghana-2018" title="Python loves GIS - PyCon Ghana 2018" target="_blank">Python loves GIS - PyCon Ghana 2018</a> </strong> from <strong><a href="https://www.slideshare.net/Enock4seth" target="_blank">Enock Seth Nyamador</a></strong> </div>