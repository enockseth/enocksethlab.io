---
title:  "Why I Choose Ghost"
date:   2015-05-20 15:51:13
tag:  ghost
category: blog
---

![Ghost logo](https://upload.wikimedia.org/wikipedia/commons/f/fa/Ghost-Logo.svg)

Greetings,

In my last post I said I'll share with you why I decided to use Ghost looking at all the free blogging platforms around, such as [Blogger](http://blogger.com), [Worpress](http://wordpress.com) and the rest. 

Yes I know, I have a blog on Blogger why I used to share my ideas and thoughts. I can login and write a blog post any where thanks to Google.

Since Ghost runs on [Node.js](https://en.wikipedia.org/wiki/Node.js), I have it set up on my laptop running Linux Mint 17.1 first.

I choose Ghost out of my quest to install and try popular [CMS](https://en.wikipedia.org/wiki/Content_management_system) *Content Management System* software [Wordpress](http://worpress.org) and [Drupal](https://www.drupal.org/) locally then the new kid Ghost popped up.

After reading about it's features I decided to try it out. It took me days to get Ghost up and running. I toured it and love its **simplicity**. Quickly I found out I can use Ghost with Github static pages. Then I thought of moving over.

One thing I like about Ghost is using **markup** for publishing, that sounds cool to me.

Also, there lot of free responsive themes available. Plus am able to customize these to my liking. I was able to get get [Disqus](http://disqus.com/) running.

Now how am I able to publish blog posts? This is the part that most people might not want to try because it's a involving a little but hey is an adventure :).

Since Ghost is on my local machine, I just spin up my local *Node.js* server to start ghost and I can access it at port *2368* on *localhost*. Whenever am done with a blog post I use the [Buster](https://github.com/axitkhurana/buster) tool to deploy to Github and that is how you're able to see and read :).

Thanks for passing by and hope you're gonna enjoy my ideas, thoughts and updates.

Cheers.
