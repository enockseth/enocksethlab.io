---
title: "Four A+F Wikipedia Saturdays in Accra, Ghana"
date: '2019-04-18 00:00:00'
tag:
- wikimedia
- wikipedia
- ghana
- arts+feminism
image: "/images/artsandfeminism_accra_group.jpg"
description: Arts and Feminism Wikipedia Session with Wikimedia Ghana in Accra 2019
---

![Arts and Feminism Group](/images/artsandfeminism_accra_group.jpg)

[Wikipedia](http://en.wikipedia.org) by far is the most used online Encyclopedia by all. I have been contributing volunteer time to Wikipedia and other Wikimedia Foundation projects; my favourite is [WikiData](https://wikidata.org) for sometime now.

Long story short, we all use and **copy** Wikipedia but the sad thing is just a handful of us give back; am refering to adding content. If you already don't know Wikipedia is made by people like you and I who use it everyday.

March is [Art+Feminism](https://en.wikipedia.org/wiki/Art%2BFeminism) month of course International Women's Day is in March too  and glad I am a March born  :D. 

Before I continue, **note that** eventhough everyone can contribute to Wikipedia it is not a **toy box**, playground or a tool for marketting and proving that you are the best blogger in Ghana or the world, and it is not a marketing tool I repeat.

![ArtAndFeminism Logo](/images/ArtAndFeminism.png)

> Art and Feminism (stylized as Art+Feminism) is an annual worldwide edit-a-thon to add content to Wikipedia about female artists. -- Wikipedia

[Wikimedia Ghana User Group](https://twitter.com/WikimediaGH) is the oldest user group in Ghana working towards shared knowledge and mission of the Global Wikimedia movement.

![Accra Edits Wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/AMP0702.jpg/800px-AMP0702.jpg)

I participated in the 4 continuous sessions as both volunteer assistant and facilitator. **Weekend 1** through to **3** was opened to both male and female  participnats. It was focused on getting both intermediate contributors up to speed and helping new contributors get started.

**Weekend 4** was dedicated to women only and Wikimedia Ghana UG is proud to have participants from [Developers in Vogue](https://developersinvogue.org/). Here is a great recap by Setor titled [Wonder Women of Wiki
](https://girlnamedsetor.wordpress.com/2019/03/29/wonder-women-of-wiki/)

Articles contributed to or created were targed at women with little or no coverage who actually met [WP:Notabiity](https://en.wikipedia.org/wiki/Wikipedia:Notability).

Interested in contributing to Wikipedia or you take photographs and will love to share these images under free ([Creative Commons Licences](https://creativecommons.org/share-your-work/)) for reuse on Wikipedia and by everyone? Then do not hesitate to get in touch with the [Wikimedia Ghana User Group](https://twitter.com/WikimediaGH) or [myself](https://twitter.com/enock4seth). Also [here](https://outreach.wikimedia.org/wiki/Bookshelf) are some great resources to get you started.

Wiki love from Accra, Ghana.

Images: [Art and Feminism 2019 - Accra](https://commons.wikimedia.org/wiki/Category:Art_and_Feminism_2019_-_Accra)
