---
title:  "National Volunteer Day 2015 - #NVDay15"
date:   2015-09-16 22:12:14
tag:   
- volunteer 
- ghana 
- gvp
---

ORGANIZE OR JOIN A NATIONAL VOLUNTEER DAY ACTIVITY AROUND   SEPTEMBER 21ST

The [Ghana Volunteer Program](http://volunteeringh.org/) (GVP) is a program being run by the [GhanaThink Foundation](http://www.ghanathink.org/), an NGO based both in Ghana and the USA. National Volunteer Day, which falls around the 21st of September each year, is our major activity. It was instituted as part of GVP in 2013. This year, the event falls within the **19th – 21st Sept 2015** time frame. The initiative aims to get as many people as possible in Ghana to volunteer their time or do community service during the **Founder’s Day** Holiday weekend.
![GVP logo](/images/GVP-logo.png)
While volunteerism is not very popular and part of our society’s fabric, many individuals and organisations volunteer occasionally. National Volunteer Day (NVDay) is to increase these numbers and unify efforts in volunteerism in Ghana. We are encouraging more people to do community service within Ghana. That’s the same spirit our founding fathers engendered within our citizenry.

**Ato Ulzen-Appiah**, the director of the GhanaThink Foundation stated that: “Volunteering is a great way to show leadership in our communities and for our nation. It’s great to see National Volunteer Day growing and becoming a yearly feature”. By having volunteer or community service activities during the 19th – 21st September time frame, we would be championing the ideals of our founding fathers and joining a national effort to work for Ghana. Volunteering demonstrates initiative and hard work, two ideals that many organisations look for while hiring. We believe that by participating in NVDay, participants would be building their CVs and professional capabilities. 

We expect several individuals and organisations to hold different activities throughout the country on the day. Last year, over 80 activities were organised and registered with us, mostly by individuals and groups that were organising volunteering activities for the first time. Their activities directly impacted the lives of thousands of people. These activities happened in 9 out of the 10 regions in Ghana and in several cities and towns. Activities ranged from skills training programs, clean up exercises, tutoring in a particular course, reading clinics for kids, blood donation drives, creating playing grounds for kids in the government schools to painting schools and faded zebra crossings. More than 300 people volunteered and their work benefitted thousands of people. The impact on the volunteers and beneficiaries was massive and was captured on social media via the #NVDay14 hashtag.

**Eleanor Asare**, the lead coordinator for the Ghana Volunteer Program said, “NVDay has not only come to stay, but it is also here to cause a positive change in our attitudes towards volunteerism and to serve as a development tool.” Make it a point to be a part of NVDay15 this year by organising and registering your event on our volunteeringh.org website. The registration of your activity will allow us to support you with more volunteers and some publicity. At the [volunteeringh.org](http://volunteeringh.org) website, you can also join volunteer activities that are being planned by others. We look forward to hearing about the incredible things you will be doing on NVDay15. Stay updated with what’s happening with the [#NVDay15 hashtag](https://twitter.com/search?q=%23NVDay15&src=typd) and on social media [Twitter](http://twitter.com/volunteeringh), [Facebook](http://facebook.com/GhanaVolunteerProgram) and Google+ . You can also support the organization of National Volunteer Day activities by donating through [gofundme.com/Nvday15](http://www.gofundme.com/Nvday15).
![NVDay](/images/NVDaykp.png)

*Volunteerism, the heart of community development.*
