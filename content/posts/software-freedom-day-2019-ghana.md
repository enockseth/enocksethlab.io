---
title: Celebrate SFD with us on September 21, 2019!
date: '2019-08-22 23:29:44'
tag:
- SFD
- FOSS
image: "/images/sfd_2019.jpg"
---

![SFD 2019](/images/sfd_2019.jpg)

Software Freedom Day (SFD) is a worldwide celebration of Free and Open Source Software (FOSS). The goal is to educate the worldwide public about the benefits of using high quality FOSS in education, in government, at home, and in business - in short, everywhere! -- [Software Freedom Day](https://www.softwarefreedomday.org/about).


SFD celebrations in Accra, Ghana over the past 15 years have been organized by [Linux Accra](https://www.linuxaccra.org/). [AITI-KACE](https://www.aiti-kace.com.gh/) and [ISOC Ghana Chapter](https://www.isoc.gh/) are supporting this year to bring SFD to Bolgatanga and Sunyani too.

 <a href="https://www.softwarefreedomday.org"><img src="https://www.softwarefreedomday.org/images/banners/web-banner-chat-we-re-organizing-h.png" alt="Organizing SFD"/>
</a>
  
AITI-KACE has opened new centers in Bolgatanga and Sunyani and as a community of practice we are extending the celebration of this year's SFD to these sites as well. The theme for this year's celebration is:

> Innovation, AI and Open Source Technology

Register [here](https://bit.ly/sfdghana19) and also stay tuned to [Linux Accra](https://linuxaccra.org) for more information. Want to support organization of SFD financially or kind? Reach out to **info@linuxaccra.org**.

Let the openness and source be with you :)
