---
title: re:publica in Accra, Ghana 2018
date: '2018-12-16 10:00:00 +0000'
tag:
- conference
- openstreetmap
- wikimedia
- republica
description: re:publica the German conference for the internet society was in Accra, Ghana and Africa #rpAccra
image: "/images/rpaccra-willy-enock.jpg"
---

re:publica the German conference for the internet society was in Accra, Ghana and Africa for the first time this December 2018.

![Wikiedia Ghana booth republica Accra](/images/wikimedia-ghana-republica.jpg)

I had 2 sessions at re:publica 2018

* [Open Maps, Open Data: Power of Accessible Geo Data](https://accra18.re-publica.com/en/session/open-data-open-maps-power-accessible-geodata)
* [Create, Contribute and Edit: Introduction to Wikidata](https://accra18.re-publica.com/en/session/create-contribute-edit-wikidata-workshop)

I spent most my time as a long time member of the [Wikimedia Ghana User Group](https://twitter.com/WikimediaGH/) volunteering at our booth engaging and talking to visitors about our movement and what we do. As usual we all know of Wikipedia but not everyone knows they can also contribute to this shared knowledge repository.

<blockquote class="twitter-tweet" data-conversation="none" data-lang="fr"><p lang="en" dir="ltr">Our stand is also open in the digital rights lounge pass by, do your first edit or learn more about <a href="https://twitter.com/Wikipedia?ref_src=twsrc%5Etfw">@Wikipedia</a> and it&#39;s sister projects.<br><br>We have stickers for everyone and also special gifts for people who make their first edits.<a href="https://twitter.com/hashtag/NowEditingWikipedia?src=hash&amp;ref_src=twsrc%5Etfw">#NowEditingWikipedia</a> <a href="https://twitter.com/hashtag/rpAccra?src=hash&amp;ref_src=twsrc%5Etfw">#rpAccra</a> <a href="https://t.co/rwyLJFN1Ah">pic.twitter.com/rwyLJFN1Ah</a></p>&mdash; Wikimedia Ghana UG (@WikimediaGH) <a href="https://twitter.com/WikimediaGH/status/1073548271635886080?ref_src=twsrc%5Etfw">14 décembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

![Willy and Enock](/images/rpaccra-willy-enock.jpg)
My [first session](https://accra18.re-publica.com/en/session/open-data-open-maps-power-accessible-geodata) was co-led with my colleague [SOB Willy-Franck](https://www.linkedin.com/in/willy-franck-sob-b400bb85/) of [SOGEFI](http://sogefi.cm/) and [OpenStreetMap Africa](https://openstreetmap.africa/). We had a diverse audience of participants where we both presented what OpenStreetMap is, how it's driving growth and development across the Africa continent and the world. We highlighted use cases and projects involving OpenStreetMap and capacity nuilding across the Africa including but not limited to:

* [OpenStreetMap and Haiti earthquake, 2010](https://www.youtube.com/watch?v=BwMM_vsA3aY)
* [Open Cities Africa](http://opencitiesproject.org/)
* [Ramani Huria](http://ramanihuria.org/)
* [Map Kibera](http://mapkibera.org/)
* [Mapping for Ebola](https://blog.mapbox.com/ebola-mapping-in-guinea-humanitarian-openstreetmap-team-3bea06639199)

Willy also displayed how SOGEFI uses OSM data and engaging with civil and government institutions in Cameroun. We had an interesting ecounter with the participants answering questions and exchanging contacts. 

You're invited to participate or support [State of the Map Africa](http://2019.stateofthemap.africa/) gathering in Grand-Bassam, Ivory Coast from 22 to 24 November 2019.

I quickly have to run for my next session which was just after Open Data, Open Maps. Want to say kudos to the rpAccra programme team for great coordinantion getting us up and running after our was changed.

[Wikidata](http://wikidata.org/) is the free knowledge base that can be contributed to by anyone. Wikidata is a structured linked database. It's a Wikimedia Foundation project just like Wikipedia but more than just text and media.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/Enock4seth?ref_src=twsrc%5Etfw">@Enock4seth</a> leading a <a href="https://twitter.com/wikidata?ref_src=twsrc%5Etfw">@wikidata</a> session at <a href="https://twitter.com/rpAccra?ref_src=twsrc%5Etfw">@rpAccra</a> day 2 <a href="https://twitter.com/WikimediaGH?ref_src=twsrc%5Etfw">@WikimediaGH</a> <a href="https://twitter.com/Wikipedia?ref_src=twsrc%5Etfw">@wikipedia</a> <a href="https://twitter.com/hashtag/rpAccra?src=hash&amp;ref_src=twsrc%5Etfw">#rpAccra</a> <a href="https://t.co/hWXIx4tWOL">pic.twitter.com/hWXIx4tWOL</a></p>&mdash; deborah dzathor (@delasiavis) <a href="https://twitter.com/delasiavis/status/1074409466458705920?ref_src=twsrc%5Etfw">16 décembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

You can find my slides for the Wiki Data session [here](https://commons.wikimedia.org/wiki/File:Wikidata_101_-_republica_Accra_18.pdf)

re:publica was a great experience. I didn't miss the social session; pre and during :)

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">re:publica Accra started with a street party! <a href="https://twitter.com/hashtag/rpAccra?src=hash&amp;ref_src=twsrc%5Etfw">#rpAccra</a> <a href="https://twitter.com/hashtag/NextLevel?src=hash&amp;ref_src=twsrc%5Etfw">#NextLevel</a> <a href="https://twitter.com/hashtag/EnterAfrica?src=hash&amp;ref_src=twsrc%5Etfw">#EnterAfrica</a> <a href="https://twitter.com/hashtag/GoetheGames?src=hash&amp;ref_src=twsrc%5Etfw">#GoetheGames</a> <a href="https://twitter.com/goethejoburg?ref_src=twsrc%5Etfw">@goethejoburg</a> <a href="https://t.co/C3fqM07YR3">pic.twitter.com/C3fqM07YR3</a></p>&mdash; Stefanie Kastner (@StefanieKastner) <a href="https://twitter.com/StefanieKastner/status/1073322459900841984?ref_src=twsrc%5Etfw">13 décembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
