---
title: 'SFD 18: Open Source for SDGs'
layout: post
date: '2018-09-13 23:00:00 +0000'
tag:
- sfd
- open source
- FOSS
category: blog
author: enockseth
image: "/images/sfd18.jpg"
---

![SFD 18](/images/sfd18.jpg)
>  Ever wondered what all these Open Source and Linux stuffs are? It's time to jail break. 

[Software Freedom Day](https://www.softwarefreedomday.org/) is a global celebration of Free and Open Source Software. Over 1000 volunteer groups in over 120 countries all around the globe will be celebrating the Software Freedom Day every year in Septermber.

Software Freedom Day is aimed to educate the public and encourage the use of free and open source software. Join us here in Accra for an educative session from a flurry of speakers, workshops, exciting projects on exhibition, see you there!

[Linux Accra](http://linuxaccra.org) is the oldest and the largest Linux user group in Ghana. We are a community made up of Open source and Linux hobbyists, professionals, enthusiasts, developers as well as newbies.
Our members share interests in Linux and other open source software and projects.

We meet every Saturday from 2pm to 5pm (14:00 - 17:00) at the Ghana-India Kofi Annan Centre of Excellence in ICT - Advance Information Technology Institute, is located at 2nd Avenue Ridge, Opposite the Council of State.

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-0.19288837909698486%2C5.558912656134394%2C-0.19116640090942386%2C5.560274147965154&amp;layer=mapnik&amp;marker=5.559593402443425%2C-0.19202754205616657" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=5.55959&amp;mlon=-0.19203#map=19/5.55959/-0.19203">View Larger Map</a></small>

It's never too late to join us this Saturday September 15, 2018! [Register for FREE now](https://sfdaccra18.eventbrite.com)!