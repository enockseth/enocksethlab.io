---
title:  "Recap of BarCamp Tema 2015"
date:   2015-05-31 09:51:51
tag:   
- events 
- barcampghana
headerImage: false
image: /images/barcamp-tema-2015.jpg

---

![BarCamp Tema 2015](/images/barcamp-tema-2015.jpg)
BarCamp Tema 2015 was a FREE networking and mentoring organized by The GhanaThink Foundation. 

It took place at the Tema Rotary Club Center. It was my first BarCamp for this year and my third time at BarCamp Tema.

The theme for this year's BarCamp Tema was:
>Create, Impact, Sustain

Speed Mentoring is *K* at BarCamps. I was with only one mentor, you know why? Because she's from [@TigoGhana](http://twitter.com/TigoGhana), Gifty Bingley. Am a long time Tigo user so I've got the opportunity ask lot of questions this time. Made it short used 5 out of 10 minutes. Asked questions related Social Media to network issues. That's my Tigo for you :).

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr"><a href="https://twitter.com/Enock4seth">@Enock4seth</a> is a <a href="https://twitter.com/TigoGhana">@TigoGhana</a> user. He has lots of questions for <a href="https://twitter.com/GiftyBingley">@GiftyBingley</a> <a href="http://t.co/jKge5ZtUq8">pic.twitter.com/jKge5ZtUq8</a></p>&mdash; Barcamp Cape Coast (@BcCapeCoast) <a href="https://twitter.com/BcCapeCoast/status/604627214202748928">May 30, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Speed Mentoring ongoing <a href="https://twitter.com/hashtag/Tema00?src=hash">#Tema00</a> <a href="https://twitter.com/hashtag/bctema?src=hash">#bctema</a> <a href="http://t.co/ikI4sLfW2d">pic.twitter.com/ikI4sLfW2d</a></p>&mdash; Barcamp Cape Coast (@BcCapeCoast) <a href="https://twitter.com/BcCapeCoast/status/604624255821692929">May 30, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

BarCamp Ghana is always matching Twitter handles to faces, networking. I believe in networking meeting new and old networks is something I love so much. Met old and new folks as well. 

![Enock, Leslie and Tigo agent](/images/leslie-enock-bctema.jpg)
Myself, Leslie and Tigo agent.

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="und" dir="ltr"><a href="https://twitter.com/Enock4seth">@Enock4seth</a> and <a href="https://twitter.com/duncanqwame33">@duncanqwame33</a> <a href="http://t.co/aojCmAy2u4">pic.twitter.com/aojCmAy2u4</a></p>&mdash; Rachel Hormeku (@MissHorms) <a href="https://twitter.com/MissHorms/status/604641156757094402">May 30, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Smiles. <a href="https://twitter.com/Ghanathink">@GhanaThink</a> members <a href="https://twitter.com/kofiemeritus">@kofiemeritus</a> <a href="https://twitter.com/mintahyoung">@mintahyoung</a> <a href="https://twitter.com/MissHorms">@MissHorms</a> <a href="https://twitter.com/Enock4seth">@Enock4seth</a> <a href="https://twitter.com/AddOnGh">@AddOnGh</a> <a href="https://twitter.com/McDavesYaw">@McDavesYaw</a> after <a href="https://twitter.com/hashtag/bctema?src=hash">#bctema</a>. <a href="http://t.co/e8NqSE3dfR">pic.twitter.com/e8NqSE3dfR</a></p>&mdash; Barcamp Tema #bctema (@Barcamptema) <a href="https://twitter.com/Barcamptema/status/605170391624105985">June 1, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

As a member of the [Ghana Volunteer Program](http://twitter.com/volunteeringh) team, I was called upon to talk about GVP. Where I also talked about [GVP's Volunteer Sign Up](http://enockseth.github.io/volunteer-sign-up-ghana-volunteer-program-gvp/).
<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Enock Seth Nyamador talks about the Ghana Volunteer Program @ Barcamp Tema. &quot;We promote volunteerism&quot;. <a href="https://twitter.com/hashtag/bctema?src=hash">#bctema</a> <a href="http://t.co/iZ3OK8fnBX">pic.twitter.com/iZ3OK8fnBX</a></p>&mdash; #volunteeringh (@volunteeringh) <a href="https://twitter.com/volunteeringh/status/604676200397697024">May 30, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Oh! That's myself and Gameli, Thanks Kajsa:
<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Byebye <a href="https://twitter.com/Barcamptema">@Barcamptema</a>! Sessions now over and <a href="https://twitter.com/gamelmag">@gamelmag</a> and friend wave me off! <a href="https://twitter.com/hashtag/bctema?src=hash">#bctema</a> <a href="http://t.co/9SLbFU7Pa1">pic.twitter.com/9SLbFU7Pa1</a></p>&mdash; Kajsa Hallberg Adu (@kajsaha) <a href="https://twitter.com/kajsaha/status/604693288667480066">May 30, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

BarCamp Tema 2015 ended with two patriotic songs:
**Arise Ghana Youth for Your Country** and the **Ghana National Anthem** in *Ga*.

Want to say thank you to all BarCamp Tema 2015 sponsors: supported by **Tigo Ghana**, **Rotaract Club of Tema**, **Making All Voices Count (MAVC)**, **BLU Telecommunications**

For more on BarCamp Tema, checkout the hashtag [#bctema](https://twitter.com/hashtag/bctema?src=hash) on Facebook, Twitter, G+ and Instagram.

 **#MoreVIM**
