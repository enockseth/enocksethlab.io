---
title: Switch Default Browser on i3wm
date: '2020-05-24 07:00:00'
tag:
- Linux
- WM
image: "/images/i3wm.jpg"
---
![i3wm](/images/i3wm.jpg)
[i3wm](https://i3wm.org/) is by far my most used Window Manager (WM), who cares about Desktop Environments (DEs) :). On DEs such as _XFCE, KDE, GNOME, LXDE, etc_ it is always by a click at a button. 

It's not the job of a WM do same since it's not a DE. Hence after a few web searches. The solution below worked for me. Since I manage my login, WM and DE choice with __.xinitrc__ all I did was to add the following lines to my __.xinitrc__

```bash
export DESKTOP_SESSION="gnome"
export DE="gnome"
```
I hope this helps!

Reference: [Reddit](https://www.reddit.com/r/i3wm/comments/5imgjv/cant_make_vivaldi_my_default_browser/)
