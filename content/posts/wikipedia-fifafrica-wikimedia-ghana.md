---
title: Talking Wikimedia at FIFAfrica18
date: '2018-10-02 07:00:00 +0000'
tag:
- Wikipedia
- Africa
- Internet
image: "/images/fifafrica18.jpg"
---

I have been a Wikipedian and Wikimedian since 2012. And a volunteer at Wikimedia Ghana User Group since. Representing at BarCamps to community events and creating awareness about shared knowledge.
<blockquote class="twitter-tweet" data-lang="en"><p lang="ca" dir="ltr">Wikipedia 101: A practical Session<a href="https://twitter.com/Enock4seth?ref_src=twsrc%5Etfw">@Enock4seth</a> <a href="https://twitter.com/hashtag/FIFAfrica18?src=hash&amp;ref_src=twsrc%5Etfw">#FIFAfrica18</a><a href="https://twitter.com/hashtag/InternetFreedomAfrica?src=hash&amp;ref_src=twsrc%5Etfw">#InternetFreedomAfrica</a> <a href="https://t.co/KiXtaG2xvo">pic.twitter.com/KiXtaG2xvo</a></p>&mdash; #WAMECA2018 IS COMING!!! (@TheMFWA) <a href="https://twitter.com/TheMFWA/status/1045707359488815104?ref_src=twsrc%5Etfw">September 28, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
I am glad to be back in action after some years of been away due to school and dedicating most of my time to [OpenStreetMap](https://www.openstreetmap.org/about) and [WikiData](https://www.wikidata.org/) :).

![FIFAfrica Groupfie](/images/fifafrica-groupfie.jpg)
During [Forum on Internet Freedom in Africa 2018](https://cipesa.org/2018/07/register-for-the-forum-on-internet-freedom-in-africa-2018-fifafrica18/) (#FIFAfrica) which took place in Accra, Ghana and brought together policy makers to enthusiasts together. Justice and I represented our usergroup by leading a session on Wikipedia and Wikimedia.

Our session focused on guiding and educating users of the world's largest Encyclopedia made by volunteers. We had 3 presentations and and Q&As
* Part I – Wikipedia Background & Editorial Principles
* Part II – Wikipedia Governance
* Part III – Communities around Wikipedia

The session was very interactive responding to questions from participants. We we're thrillied to also have Dorothy K. Gordon Ghanaian technology activist and development specialist participate in our session.
<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">It was an interesting session later today as we held our <a href="https://twitter.com/Wikipedia?ref_src=twsrc%5Etfw">@Wikipedia</a> 101: &quot; A Practical Session&quot; presentation led by <a href="https://twitter.com/Enock4seth?ref_src=twsrc%5Etfw">@Enock4seth</a> and <a href="https://twitter.com/Owula_Kpakpo?ref_src=twsrc%5Etfw">@Owula_Kpakpo</a> at the <a href="https://twitter.com/hashtag/InternetFreedomAfrica?src=hash&amp;ref_src=twsrc%5Etfw">#InternetFreedomAfrica</a> forum in Accra. Thanks for coming to our presentation.<a href="https://twitter.com/hashtag/FIFAfrica18?src=hash&amp;ref_src=twsrc%5Etfw">#FIFAfrica18</a> <a href="https://twitter.com/hashtag/NowEditingWikipedia?src=hash&amp;ref_src=twsrc%5Etfw">#NowEditingWikipedia</a> <a href="https://t.co/QYFcANOo0Q">pic.twitter.com/QYFcANOo0Q</a></p>&mdash; Wikimedia Ghana UG (@WikimediaGH) <a href="https://twitter.com/WikimediaGH/status/1045767108255191040?ref_src=twsrc%5Etfw">September 28, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
Participants were excited about what WMGHUG and the work global community does and signed up to join and learn more about contributing to this pool of knowledge.

You are always welcome to join [WMGHUG](
http://www.wmgh.org)! Sign up here and start editing. Thanks to all for attending. You can watch recorded session from [Facebook Live](https://www.facebook.com/WikimediaGhanaUG/videos/461411974366713/).