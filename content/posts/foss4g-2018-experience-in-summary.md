---
title: My FOSS4G 2018 Experience in a nutshell
date: '2018-09-13 22:11:00 +0000'
tag:
- foss4g
- conference
- opencitiesafrica
image: "/images/foss4g2018_banner.jpg"
---

FOSS4G is the annual largest global conference bringing together Open Source geospatial ethusiasts, hackers, developers and everyone together. [FOSS4G 2018](https://2018.foss4g.org) took place in Dar es Salaam, Tanzania. This was my first FOSS4G as a participant and member of the Local Organizing Committee (LOC). 

![FOSS4G2018 Banner](/images/foss4g2018_banner.jpg)

This was [my second time travelling to Dar](/hot-activation-workshop-dar-es-salaam/) and same venue for FOSS4G, Julius Nyerere International Convention Centre. It was a déjà vu. Meeting new, old and virtal friends Away From Keyboard (AFK). 

Happening alongside FOSS4G 2018 was **HOT Summit**, **Open Cities Africa 2nd Regional meeting** and **Understanding Risk in Tanzania.**

As a member of LOC, there was no time to sit in any session or workshop but snippets of them. Pleasure it was for me, LOC and FOSS4G volunteers to have made everyones participation a memorable one.  FOSS4G 2018 was all about you and leaving no-one behind.

![FOSS4G Info Desk](/images/foss4g_info_desk.jpg)

FOSS4G is not always about workshops and talks. Time to dance and celebrate community members for sacrificing their times. So far as there is music you got to make a move.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/kateregga1/43644020854/in/pool-foss4g/" title="ZB9A5770"><img src="https://farm2.staticflickr.com/1867/43644020854_2161fa503f_z.jpg" width="640" height="426" alt="ZB9A5770"></a><script async src="//embedr.flickr.com/client-code.js" charset="utf-8"></script>

Congratulations once again to Astrid for winning this year's Sol Katz Award. I owe you a bowl of [fufu](https://en.wikipedia.org/wiki/Fufu) 😎😎

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Captured moment with 2018 Sol Katz Award winner <a href="https://twitter.com/foss4g?ref_src=twsrc%5Etfw">@foss4g</a>, Congrats <a href="https://twitter.com/astroidex?ref_src=twsrc%5Etfw">@astroidex</a>!  <a href="https://twitter.com/OSGeo?ref_src=twsrc%5Etfw">@OSGeo</a> love from <a href="https://twitter.com/hashtag/FOSS4G2018?src=hash&amp;ref_src=twsrc%5Etfw">#FOSS4G2018</a>🌍😍. Thanks <a href="https://twitter.com/jodygarnett?ref_src=twsrc%5Etfw">@jodygarnett</a>. <a href="https://twitter.com/hashtag/FOSS4G?src=hash&amp;ref_src=twsrc%5Etfw">#FOSS4G</a> <a href="https://t.co/JXb88O6gdI">pic.twitter.com/JXb88O6gdI</a></p>&mdash; Enock Seth Nyamador #NVDay18 (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/1038859391280275457?ref_src=twsrc%5Etfw">9 septembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I want to say a big thank you to FOSS4G 2018 LOC, Sponsors, Volunteers and all participants. You are wonderful.

![FOSS4G Volunteers](/images/foss4g_volunteers.jpg)

Checkout [@foss4g](https://twitter.com/foss4g) and [#foss4g2018](https://twitter.com/hashtag/foss4g2018) for more. If you have images from FOSS4G, kindly join and share on the [FOSS4G Flickr Group](https://www.flickr.com/groups/foss4g/). 

Looking forward to future regional and country based FOSS4Gs across Africa; not forgetting hosting FOSS4G in Ghana :) next time FOSS4G returns to Africa :). See you soon.

Asante sana!