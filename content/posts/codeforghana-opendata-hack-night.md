---
title:  "[CodeforGhana] OpenData Hack Night"
date:   2015-08-06 07:53:25
tag:   
- ghana 
- events
- open-data
---

![](/images/opendatahacknight.jpg)

Greetings. 

Over the weekend (*31 July - 1 August*) myself and other Ghanaian OpenData enthusiasts and hackers were present at the OpenData Hackathon organized by [CodeforGhana](http://codeforghana.org) and ~~[NITA](http://www.nita.gov.gh/)~~. 

It was great and fun but one thing I really was disappointed about was the absence of persons from NITA. I was expecting them as the hackathon was in partnership with them. I really had lot of questions for them though.
![](/images/nita-absent.png)

It was all about OpenData and most datasets used were downloaded from [Data.gov.gh](http://data.gov.gh).

The hackathon was an overnight one. Even though there were frequent [Dumsors](http://enwp.org/Dumsor), the organizers did well to always provide power. I guess it was because we were visualizing Wikipedia Dumsor stats with R :). 

Now what and what happened? CodeforGhana have said all [here](http://www.codeforghana.org/2015/07/30/opendatahacknigt.html).

There were 7 teams in all. I will like to talk about **Team FireViz** which I was part of.

TeamFireViz was made of Carl, Myself, Selom and Nathaniel. Carl wasn't able to comeback the next as we presented our finished FireViz. Carl was very supportive, he had awesome ideas.

[Selom](http://twitter.com/banybah) a Developer from Coders4Africa , [Nathaniel](http://twitter.com/natalpha27) our Journalist :), and [Enock](http://twitter.com/enock4seth) (myself) the Map Maker.

FireViz (Fire Visualization) took dataset containing fire outbreaks in Ghana for 2010 and projected it over the Ghana map. The fires were in different categories from Domestic to Industrial. You can have a view of what we did [here](http://fireviz.pe.hu/) the source code will be pushed to [GitHub](http://gitbub.com) soon.
![](/images/ghana-fireviz.png)

At the end of the two days, Team 7 emerged winners. What of FireViz :)? We were second with two other teams.

Glad to meet new and old faces. Not to forget, I met Masssly almost 2 years knowing each other as Wikipedians virtually. It was nice having Afua Ankomah around too.

Finally I will to say thanks to CodeforGhana for bringing this up.  And to all OpenData enthusiasts in Ghana #MoreVIM.

Thanks.
