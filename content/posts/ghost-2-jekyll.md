---
title:  "Ghost 2 Jekyll"
date:   2017-04-14 02:56:36
tag:   
- jekyll 
- ghost
image:	/images/jekyll-logo-light-solid.png
---

![Jekyll](/images/jekyll-logo-light-solid.png)

Hello Readers,

Is been a while. Lost control of my local [Ghost](https://enockseth.github.io/welcome-to-my-new-blog/) environment and have been lazy to work it out.

Today am switching over to Jekyll. It was easy and quick to setup. Forking `Indigo` theme and using `ghost2jekyll.py` to generate jekyll posts from `ghost-dev.db`. 

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Finally ~`jekyll serve` works 😀 Time to leave ghost <a href="https://twitter.com/JeanHuit">@JeanHuit</a> 😉</p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/850728107623538688">8 avril 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Goodbye Ghost it was fun using and messing aroud you.