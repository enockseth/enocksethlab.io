---
title: 2018 in Review
date: '2018-12-31 21:37:00 +0000'
tag:
- year-review
image: "/images/2018_fireworks_purple.jpg"
---
![2018](/images/2018_fireworks_purple.jpg)

> A journey of a thousand miles begins with a single step -- Laozi

2018 has been a great, open and year of communities of practise for me. As I sit here in my grandmothers house in [Aflao](https://en.wikipedia.org/wiki/Aflao) where I was born. I have been away for long, as my mum always do on every New Year's Eve we go to church at St. Benedictus RC Church, Viepe. Guess what happened when I called her that am home; she said hurry and come to church. My mum is one very dedicated Christian. So let make this short and join her :).

Too much of the long talk. This year I completed my Bachelors degree at the University of Mines and Technology, Tarkwa where I have spent most of the past 4 years. Don't ask me about the graduation pictures :-). Still puting a post together about my first degree experience. 

![Ranccho](/images/aamir-khan-rancho-759.jpg)
Source: indianexpress.com

Was part of the team that brought [FOSS4G 2018](/foss4g-2018-experience-in-summary/) to you in Dar Es Salaam. I also participated in the first regional Open Cities Africa workshop in Kampala, Uganda.

![OSMAfrica Crew at OpenCitiesAfrica, Kampala](/images/osmafrica-opencitiesafrica.jpg)
[OSMAfrica Crew at OpenCitiesAfrica, Kampala](https://twitter.com/kateregga1/status/1006176617947979777)

Fast forward, I started my National Service which is required of every Ghanaian graduate by law at the Accra Metropolitan Assembly Department of Transport at the same started working on [Open Cities Africa](http://opencitiesproject.org/) in Accra.

As the Online Lead at the Ghana Volunteer Program, 2018 also saw a wonderful number of volunteer activities on [National Volunteer Day](/nvday18-ghana-volunteer-program/).

I also attended and spoke at some events this year:
* [SSATP, Abuja](https://www.ssatp.org/)
* [PyCon Ghana](/python-loves-goe-pycon-ghana/)
* [FIFAfrica, Accra](/wikipedia-fifafrica-wikimedia-ghana/)
* [re:publica, Accra](/speaking-at-republica-accra-2018/)
* [Mapbox Workshop, Zanzibar](/zanzibar-mapbox-workshop/)
* [Software Freedom Day 18, Accra](/software-freedom-day-18-accra-ghana/)
* [DigitalTransport4Africa, Paris](/paratransit-workshop-paris-ii/)

I also happen to have traveled to Sunyani for the first time year. Looking forward to a mappy 2019, travelling to 10 regions of Ghana (I guess I have to make the 10 before the sub divisions :). See you soon and thanks for reading.

![2019](/images/800px-Loading_2019.png)