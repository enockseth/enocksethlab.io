---
title: Zanzibar Mapbox Workshop
date: '2018-09-14 10:00:00 +0000'
tag:
- foss4g
- mapbox
- travel
- opencitiesafrica
image: "/images/zmi-drone.jpg"
---

Right before [FOSS4G 2018](/foss4g-2018-experience-in-summary/) closing ceremony I had to join the [Open Cities Africa](https://opencitiesproject.org/) team en route [Zanzibar](https://en.wikipedia.org/wiki/Zanzibar).

First time travelling from Dar es Salaam to Stone Town and you know right; ferry of course :D and it was my first ferry ride too.
![Stone Town](/images/zanzibar.jpg)

The purpose for travelling to Zanzibar was for a 2 day [Mapbox](https://mapbox.com) workshop on builing web maps. Thanks to the Zanzibar Mapping Initiative (ZMI) we had great geospatial data from high definition drone images to spreadsheet data combined with yours truly OpenStreetMap for our workshop.

We divided ourselves into teams. My team after 2 days of collaboration came up with [this](https://hills95.github.io/Zanzibar-culture/) to help tourists find information about cultural centres in Zanzibar.

I also played around with drone image for Alogboshie, Ghana one of Open Cities Accra study areas. Take a look [here](https://enockseth.gitlab.io/oca-alogboshie).

I enjoyed every bit of Stone Town from our guided tour, night life at the [Forodhani Gardens](https://en.wikipedia.org/wiki/Forodhani_Gardens) and watching locals dive into the Indian Ocean; secret there is free Wi-Fi in the garden :D. Stone Town is a lovely place and looking forward to returning soon for more.

![Zanzibar Smiles](/images/zan-smiles.jpg)

<blockquote class="twitter-tweet" data-lang="fr"><p lang="tl" dir="ltr">Good bye Stone Town ! Hakuna matata !  Asante sana <a href="https://twitter.com/GFDRR?ref_src=twsrc%5Etfw">@GFDRR</a> @Data4Resilience <a href="https://twitter.com/Mapbox?ref_src=twsrc%5Etfw">@Mapbox</a> <a href="https://twitter.com/hashtag/FOSS4G2018?src=hash&amp;ref_src=twsrc%5Etfw">#FOSS4G2018</a> <a href="https://t.co/F8yx5pl1Yr">pic.twitter.com/F8yx5pl1Yr</a></p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/1036276182256377857?ref_src=twsrc%5Etfw">2 septembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Also checkout [Building web maps in Zanzibar](https://blog.mapbox.com/building-web-maps-in-zanzibar-c38eb6b3ce8b) on Mapbox blog
