---
title: DigitalTransport4Africa Meetup, Paris
date: '2018-11-14 10:00:00 +0000'
tag:
- transportation
- afd
- gtfs
- openstreetmap
description: Second DigitalTransport4Africa meeting and workshop in Paris
image: "/images/digitaltransport4africa.png"
---

In November 2018, I was privileged to have parcipated in the second [DigitalTransport4Africa](http://digitaltransport4africa.org/) meet-up and workshop organized by the [Agence française de développement (AFD)](https://www.afd.fr/fr) and it's partners.

![Day 0 with Nathalie](/images/enock-nathalie-paris.jpg)
<small>Day 0: Nathalie and I finding our way to the nearest KFC</small> 

My participation in this meetup dates back to mapping of [Accra's trotro](http://data.afd.fr/accramobility/) with support from the local [OpenStreetMap community](http://osmghana.org/), [Jungle Bus](http://junglebus.io) and the [Accra Metropolitan Assembly](https://ama.gov.gh).

This meetup also saw the launch of the Beta version of the [DigitalTransport4Africa Resource Center](https://digitaltransport4africa.org): which contains data, information and tools used to accomplish paratransit mapping projects in Africa and around the world.  

We had a day of Open event for everyone with presentations, interactions and networking around Open Transport at __Le Square__. [GTFS](https://en.wikipedia.org/wiki/General_Transit_Feed_Specification) was much higlighted as we are talking about transportation. Interested in GTFS? Here is a comprehensive MOOC from the World Bank - [Introduction to the General Transit Feed Specification (GTFS) and Informal Transit System Mapping (Self-Paced)](https://olc.worldbank.org/content/introduction-general-transit-feed-specification-gtfs-and-informal-transit-system-mapping) 

Knowing that this was my first time in Europe and Paris, I had the opportunity to move around a bit. I loved the connectivity of sidewalks in Paris. Thanks to the awesome OpenStreetMap community in France, my self and colleague enthusiasts had no difficulty moving around. It was also a great time to put face to somr OSM usernames from Paris I have known remotely.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">OSM team preparing next projects of transport mapping in West Africa <a href="https://t.co/r335gkcRzF">pic.twitter.com/r335gkcRzF</a></p>&mdash; Antoine Chevre (@AC2N1) <a href="https://twitter.com/AC2N1/status/1063048753442144256?ref_src=twsrc%5Etfw">15 novembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

![Walking Paris](/images/walk-paris.jpg)
<small>I very much admire how easy it is to walk around in Paris. Yes I walked from my around Gare de Lyon to Notre Dame then to Eiffel Tower with Mr. Randy :)</small>

![Cheers!](/images/community-transport.jpg)
<small>Cheers! Am not good at selfies!</small>

![Kisio Digital](/images/navitia-kisio.jpg)
<small>We visited Kisio Digital roof top</small>

![La Defense](/images/metro-defense.jpg)
<small>En route to visit Jean-Marc, first time riding a metro :D</small>

![Get lost!](/images/lost-in-paris.jpg)
<small>When we got lost on a freezing morning.</small>

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">Merci à <a href="https://twitter.com/Honorable_Nath?ref_src=twsrc%5Etfw">@Honorable_Nath</a> pour cette Belle rencontre avec <a href="https://twitter.com/liotier?ref_src=twsrc%5Etfw">@liotier</a> et  <a href="https://twitter.com/Enock4seth?ref_src=twsrc%5Etfw">@Enock4seth</a> mon voisin d&#39;à côté que je connaissais que sur Twitter. &quot;je m&#39;attendais à voir 1 grand monsieur mais j&#39;étais surprise de voir que c&#39;était une copie de <a href="https://twitter.com/ndongamadu?ref_src=twsrc%5Etfw">@ndongamadu</a>  et <a href="https://twitter.com/AKEAmazan?ref_src=twsrc%5Etfw">@AKEAmazan</a> 😊😎&quot; <a href="https://t.co/ngsl4BIOxw">pic.twitter.com/ngsl4BIOxw</a></p>&mdash; AIMEE SAMA (@aimee_sama) <a href="https://twitter.com/aimee_sama/status/1063894106051616770?ref_src=twsrc%5Etfw">17 novembre 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Thank you to Agence Francaise de Developpemnent for the invitation and all [DigitalTransport4Africa](http://digitaltransport4africa.org/) partners for organizing this meet up. 