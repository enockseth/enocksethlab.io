---
title: Software Freedom Day 18 in Accra, Ghana
date: '2018-09-15 13:00:00 +0000'
tag:
- SFD
- OSGeo
- FOSS
image: "/images/sfd-group.jpg"
---

SFD is a worldwide celebration of Free and Open Source Software (FOSS). The goal is to educate the worldwide public about the benefits of using high quality FOSS in education, in government, at home, and in business - in short, everywhere! -- [Software Freedom Day](https://www.softwarefreedomday.org/about)

In Accra [SFD](https://www.softwarefreedomday.org/about) have been organised by the [Linux Accra Users Group](http://linuxaccra.org/) for more than 5 years.

![Sabra Asante](/images/sabra-asante.jpg)
Mr. Asante one the founding and very supportive members' of Linux Accra started the day with a talk about why Software Freedom to open source doesn't mean you don't get money or less insecure software. He talked about community and RSM's 4 levels Openess and Freedom:

> 0 - Freedom to run the program as you wish.

> 1 - Freedom to study the source code of the program and then change it so the program does what you wish.

> 2 - Freedom to help your neighbour. That’s the freedom to redistribute the exact copies of the software when you wish.

> 3 -Freedom to contribute to your community. That’s the freedom to distribute copies or modified versions when you wish.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vFoyyhNVVSQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
We took a quick bite of the OpenStreetMap segment of [Episode 1: Even Big Data Starts small](http://crowdandcloud.org/watch-the-episodes/episode-one).

![Radhika Lal](/images/radhika-lal.jpg)
Radhika Lal, Economic Advisor at UNDP, Ghana got everyone to introduce themselves. She continued on the importance of Open Source to all sectors and fields of work and to her as an economist as well. 

She talked extensively about the power of open data and open source citing examples; from all of the great works in the world wouldn't have been accomplished in without Open Source; Department of Defense in the US, many government institutions, security agencies and several big companies use OS; telling you is not only for hobbyist.

She also talked about OpenStreetMap being a very important tool in planning and advised institutions on making information and data accessible to the public as Open Data for reuse and development.

She emphasized on the need for more communities around open source and finding the balance our various communities. 

She concluded with the popular saying: enough people, enough eye balls the world can change.

There were series of presentations, covering topics and tools below:

* [Mifos](http://mifos.org/)
* [SuiteCRM](https://suitecrm.com/)
* [OpenStreetMap](http://openstreetmap.org/)
* [Visuals4Gender](http://visualsforgender.org/)
* Gaming on Linux
* [Drupal](http://drupal.org/)
* [Open Cities Africa](http://opencitiesproject.org/)
* [OSGeo](http://osgeo.org/)
* [Wikimedia](https://www.wikimedia.org/)
* Internet of Things (IoT)

We had 2 panel discussions;
* Open Source, Security and the SDGs
* The importance of Data and open source in Achieving the Sustainable development goals (SDG’s)

![SFD 18 Accra group photo](/images/sfd-group.jpg)
We were supposed to have afternoon sessions filled with workshops but with time not on our side, they were moved to Linux Accra weekly meet-ups. Feel free to walk into the [Ghana-India Kofi Annan Centre of Excellence in ICT](https://en.wikipedia.org/wiki/Ghana-India_Kofi_Annan_Centre_of_Excellence_in_ICT) every Saturday from 14H00 to 17H00 as we talk and share Openness. Thanks you all for supporting and coming.

