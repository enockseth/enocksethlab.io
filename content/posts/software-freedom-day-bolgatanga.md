---
title: Software Freedom Day 2019 in Bolgatanga
date: '2019-12-31 19:20:00'
tag:
- SFD
- Bolgatanga
category: blog
image: "/images/images/sfd_ghana_bolgatanga_2019.jpg"
---

![SFD 2019](/images/sfd_ghana_bolgatanga_2019.jpg)

Travelling by road is fun and sometime not depending on the distance, time and comfort. On September 19, I left Accra heading towards Bolgatanga for the second time in my life, the first time was through [our mappy and adenterous trip across Ghana](/traveling-ghana-open-source-way/). 

Sitting in a bus for 14 good hours on bumpy and smooth roads for the love of [Free and Open Source Software](https://en.wikipedia.org/wiki/Free_and_open-source_software) (FOSS) and spreading it. Myself and Nii Okai represented [Linux Accra](https://linuxaccra.org) in Bolga; supporting and helping bring  the celebration of FOSS to Bolga and the Northern part of Ghana for the first time.

As part of the celebrations; we had presentations featuring varioius Open Source tools an uses in several sectors.

Mr. Mumuni of [SEND Ghana](https://sendwestafrica.org/nu/gh/) gave an interesting and quick keynote on how they as practisioners have been consumers of FOSS especially [OpenDataKit](https://opendatakit.org/) for over a decade. 

* Nii Okai also gave a detailed and broad presentaion on FOSS in some selected sectors, 
*  Abusco from **[LECLARA and Laranbanga Mapping Initiativ](https://www.hotosm.org/projects/leclara-sustainable-tourism-through-local-mapping-in-ghana/)** talked OpeStreetMap for toursinm and development in Larabanda, 
* myself presented the [Open Cities Africa](https://opencitiesproject.org/) project in Accra and how we made use of only FOSS and not forgetting your free online encyclopaedia Wikipedia.

![SFD Bolgatanga Ubuntu Installation](/images/sfd_bolgatanga_ubuntu_install.jpg)

We had 3 breakout session focussing on:
* Basic Lunux and FOSS
*  Using FOSS for Bussiness 
*   Advanced Linux, Distro installations and OpenSttreetMap

Glad we could make impact through sharing! 

Let the freedom be with you :)