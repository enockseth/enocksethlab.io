---
title:  "My 2015 in Summary"
date:   2015-12-31 06:28:22
categories:	["Conference"]
---


![Some HOT members jump](/images/hot-aodc-dar-jump.jpg)

One local proverb in [*Ewe*](https://en.wikipedia.org/wiki/Ewe_language) states: "*Even if it takes years it definitely will be called tomorrow*". **December 31** is that day of the year that is farthest from **January 1**, it is rather the opposite between January and December.

2015 was not an easy year as well. It was rough and smooth. 

My 2015 was a year of getting addicted to [the Open Source Way](http://www.theopensourceway.org/) in every aspect of my life.

Looking at my school life; 2 semesters have passed away in school this year. Level 200 first semester was the toughest for me though; lot of pressure for marks. I didn't 'shark' (*learning school materials for hours in preparation of assessment; exams or quizzes*). Lot of procrastination. As a member of the Chamber of Mines Hall Army Cadet Corps, two of us were privileged to have been asked to stay at [Gold Refinery Hall](http://enockseth.github.io/life-at-gold-hall-umat/) as Reps for the 2015/16 academic year. (*more on this soon*) 

This year [Ghana Volunteers Program](https://twitter.com/volunteeringh) organized another National Volunteer Day as usual to encourage volunteerism among Ghanaians. If you missed out [NVDay15](https://twitter.com/hashtag/NVDay15?src=hash) then you should start preparing for #NVDay15 also checkout hastag #NVDay16. This is [what took place in Tarkwa](http://www.engineerskasa.com/national-volunteer-day-nvday15-in-tarkwa/).

2015 was a year of OpenStreetMap too. 

 
> "Many OSMers do what they do because they like maps in the abstract – the challenge of mapping the world is motivation enough. It turns out that most people don’t think like we do. Most folk want to know why they are mapping, what positive outcome will result from them mapping. These young mappers knew why they were mapping. - Dermot, [MapLesotho](https://maplesotho.wordpress.com/2015/12/07/90-young-mappers-and-one-flabbergasted-older-one/)

Thanks to [CodeforGhana](http://codeforghana.org/) we had the [first gathering of people in Ghana for an intro to what OpenStreetMap is](http://www.codeforghana.org/2015/06/30/openstreet.html). I also had the opportunity to have participated in the first [Humanitarian OpenStreetMap Team](http://hotosm.org) (HOT) [Activation Workshop](https://hotosm.org/updates/2015-09-08_activation_workshop_at_africa_open_data_conference) in Dar Es Salaam during the first ever [Africa OpenData Conference](http://www.africaopendata.net/). Had the opportunity to work online preparing for this and also not to forget the people I met.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">One of the great benefits of the <a href="https://twitter.com/hashtag/africaopendata?src=hash">#africaopendata</a> conference has been meeting people like <a href="https://twitter.com/Enock4seth">@Enock4seth</a> in person. <a href="http://t.co/4KZ7wXCqSi">pic.twitter.com/4KZ7wXCqSi</a></p>&mdash; David Selassie Opoku (@sdopoku) <a href="https://twitter.com/sdopoku/status/639812546703302657">September 4, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Discussing <a href="https://twitter.com/hashtag/african?src=hash">#african</a> <a href="https://twitter.com/hashtag/openstreetmap?src=hash">#openstreetmap</a> communities with <a href="https://twitter.com/Enock4seth">@Enock4seth</a> <a href="https://twitter.com/kateregga1">@kateregga1</a> justin from <a href="https://twitter.com/hashtag/malawi?src=hash">#malawi</a> <a href="https://twitter.com/hashtag/africaopendata?src=hash">#africaopendata</a> <a href="http://t.co/IOUknGX1zy">pic.twitter.com/IOUknGX1zy</a></p>&mdash; amadu (@ndongamadu) <a href="https://twitter.com/ndongamadu/status/639752640436543488">September 4, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/Enock4seth">@Enock4seth</a> at <a href="https://twitter.com/hashtag/ramanihuria?src=hash">#ramanihuria</a> <a href="https://twitter.com/OSM_TZ">@OSM_TZ</a> stand <a href="https://twitter.com/hashtag/africaopendata?src=hash">#africaopendata</a>   Great job guys really <a href="http://t.co/8DZ8MTqV21">pic.twitter.com/8DZ8MTqV21</a></p>&mdash; amadu (@ndongamadu) <a href="https://twitter.com/ndongamadu/status/639749421899612160">September 4, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Great talk of <a href="https://twitter.com/Enock4seth">@Enock4seth</a> on open data communities in Ghana, is his enthusiasm showing a generational gap? <a href="https://twitter.com/hashtag/africaopendata?src=hash">#africaopendata</a></p>&mdash; Jorieke (@Zjwarie) <a href="https://twitter.com/Zjwarie/status/640088457445044224">September 5, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


Also want to say a BIG thank you to [Severin Menard](https://twitter.com/sev_hotosm) for voluntarily visiting us in [UMaT, Tarkwa](http://umat.edu.gh) to share his knowledge with us on [OpenStreetMap](http://osm.org), [QGIS](http://qgis.org) and other GIS related stuffs. His presence was one opportunity of it's kind. Sev also continued to Accra for [Beyond OSM Workshop](http://www.codeforghana.org/2015/10/23/openstreetmap.html) in Accra, thanks to [CodeforGhana](http://www.codeforghana.org) again.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Glad to have <a href="https://twitter.com/sev_hotosm">@sev_hotosm</a> in <a href="https://twitter.com/UMaT_edu_GH">@UMaT_edu_GH</a> for an intro to <a href="https://twitter.com/hashtag/OpenStreetMap?src=hash">#OpenStreetMap</a> workshop. cc <a href="https://twitter.com/OSMGhana">@OSMGhana</a> <a href="http://t.co/qD0sXb8Rj5">pic.twitter.com/qD0sXb8Rj5</a></p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/654996481708044288">October 16, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

[engineerskasa.com](http://engineerskasa.com) is the first blog to talk about students activities in UMaT. Being an engineering only institution doesn't mean we can't do stuffs like this. For that reason, a group of us came up with this. It launched in September and as of today it's been so far so good. I'll like to say thank you to all team members.

Talking about BarCamps, this year I attended only 2 Barcamps: [Tema](http://enockseth.github.io/first-openstreetmap-workshop-ghana/) and Takoradi. BarCamp Takoradi 2015 was my first one in Takoradi. I had the opportunity to organize a break session on Volunteering and Wikipedia, Co-hosted by [Reuben](https://twitter.com/RGBekoe) and I; Reuben rode bicycle across Ghana to raise funds for The Cleft Foundation.

Not to forget my Peace Corps Ghana friends especially Michael for inviting me to Girls Leading Our World / Boys Respecting Others (GLOW/BRO) camp at Abor Senior High School. I enjoyed talking to the students.
![Peace Corps Ghana GLOW/BRO](/images/peace-corps-ghana-glow-bro.jpg)

Meeting new people, I love to meet new people everywhere I go. I really met a lot of people this year. Thanks to Gold Hall (UMaT), BarCamps, Rotaract Club of UMaT, Humanitarian OpenStreetMap Team, OSM, Wikimedia Ghana User Group, GhanaThink Foundation, Peace Corps Ghana. And those I couldn't meet see you soon.

This year wouldn't have be so without failures and NOs from others. I'll to say thank you too.

Wishing you all a **Happy New Year 2016**. See you on the other side.
