---
title: Pycon Africa 2019 in Ghana
date: 2019-08-23
tag:
- Pycon Africa
image: "/images/pycon_africa_19.jpg"
categories: ["Conference"]
---

![Pycon Africa 2019](/images/pycon_africa_group.jpg)

First edition of [Pycon Africa](https://africa.pycon.org/schedule/) took place in Accra in August 2019.

![Enock Pycon Africa](/images/enock_pycon_africa.jpg)

I had the opportunity to talk about Python and GIS Development with my talk titled "**Geo+Python: A Shallow Dive into GIS Development**". This basically a quick introduction to GIS for Python developers and also highlighting Open Source Geospatial tools and packages that can be used by PyDevs in their projects. 

The presention consisted mostly:
* Introduction to GIS; Data types, projections, etc
* [OSGeo](https://www.osgeo.org/): QGIS, GDAL, GeoNode, etc 
* [OpenStreetMap](https://osm.org); the free and colloborative map of the world
* Some geospatial libraries

Find my slides [here](/slides/geo+python/)