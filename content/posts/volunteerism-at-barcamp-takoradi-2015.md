---
title:  "Talking Volunteerism @ BarCamp Takoradi 2015"
date:   2015-10-20 19:14:24
tag:   barcampghana
---

Greetings, 

This post has taken too long to be put together not because I decided to to so intentionally but conditions beyond my control but rest assured all is up and running.

BarCamps in Ghana are free networking and mentoring events that provide people; especially youths and entrepreneurs the platform to shawcase themselves, what they're doing and meet others of same interest. 

I was at BarCamp for three reasons

- Met old and new networks
- To talk about Wikipedia and other Wikimedia Foundation projects
- To get mentored

It was great to meet new and old folks. 

My journey started from Aflao, have to spend the night over in Accra to continue to Takoradi the next morning. 

At BarCamp Takoradi I organized a breakout session focused on Volunteerism and Contributing to Wikipedia and other Wikimedia Foundation projects; joint led by myself and Ruben; A volunteer cyclist for the Cleft Foundation in Ghana. To me this was a perfect combination. Wikipedia contributors are volunteers so talking about the need to volunteer tells how important volunteers can transfer Ghana and the whole world. 

I shared my story as Volunteer contributor to Wikipedia and what it has thaught me. Rueben on the other hand also shared how he rides bycycle around Ghana as a volunteer to raise funds for the Cleft Foundation. He's volunteering for a great. He loves what I do as a contributor to great free knowledge that humanity has ever had; Wikipedia. Members of the session's expectation were met at the end of the session. One thing I point was Takoradi's Wikipedia article. Takoradi is home to several popular Ghanaian musicians and nothing on the article describes it.

Ruben, Georgina and I also used this opportunity to talk about benefits of volunteering as it can't be thought in the classroom. Volunteering is one thing that can transform this world.

Thanks for passing by. 
