---
title: Traveling and Mapping Ghana the Open Source Way
date: '2019-07-30 00:00:00'
tag:
- OpenStreetMap
- Travel
description: We travelled across Ghana the Open Source creating open data and street-level
  images on OpenStreetMap and Mapillary
image: "/images/open_travellerz.jpg"
---

![Open Travellerz](/images/open_travellerz.jpg)

In June 2019, five of us; Mawtor, Joe, Enock, Angela and Yvonne travelled across Ghana, in all to:

- capture street-level images and make them accessible to all - we got featured by [Mapillary](https://blog.mapillary.com/update/2019/06/20/map2020-tackling-humanitarian-challenges.html)
- collect POIs into OpenStreetMap
- lead capacity building in Larabanga - *detailed post for another day*
- see other parts of Ghana
- document the state of the famous Eastern Corridor Road
- have fun!

This twitter thread will tell you all:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Mappy trip from <a href="https://twitter.com/hashtag/Accra?src=hash&amp;ref_src=twsrc%5Etfw">#Accra</a> - <a href="https://twitter.com/hashtag/Laranbanga?src=hash&amp;ref_src=twsrc%5Etfw">#Laranbanga</a> then back to Accra through Eastern Corridor road capturing street-level images <a href="https://twitter.com/mapillary?ref_src=twsrc%5Etfw">@mapillary</a> + <a href="https://twitter.com/openstreetmap?ref_src=twsrc%5Etfw">@openstreetmap</a> &amp; <a href="https://twitter.com/hashtag/FOSS?src=hash&amp;ref_src=twsrc%5Etfw">#FOSS</a> love.<br>3 👬 <a href="https://twitter.com/stevdok?ref_src=twsrc%5Etfw">@stevdok</a> <a href="https://twitter.com/amuzugabrieljoe?ref_src=twsrc%5Etfw">@amuzugabrieljoe</a><br>2 👭 <a href="https://twitter.com/yaa2darko?ref_src=twsrc%5Etfw">@yaa2darko</a> <a href="https://twitter.com/teyvi_angela?ref_src=twsrc%5Etfw"> @teyvi_angela</a><br>1 Garmin Virb X 📷 <br>1 🚗<a href="https://twitter.com/hashtag/map2020?src=hash&amp;ref_src=twsrc%5Etfw">#map2020</a> <a href="https://twitter.com/hashtag/OpenData?src=hash&amp;ref_src=twsrc%5Etfw">#OpenData</a> <a href="https://twitter.com/hashtag/OpenCitiesAffica?src=hash&amp;ref_src=twsrc%5Etfw">#OpenCitiesAffica</a> <a href="https://t.co/MafBTlunAj">pic.twitter.com/MafBTlunAj</a></p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/1136986639006412801?ref_src=twsrc%5Etfw">June 7, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

We spent one week on the road. Most of the driving was done by Mawutor our Chief driver. We captured in all over **300 GB** of street level images with Garmin Virb X camera. All have been uploaded to [Mapillary](https://mapillary.com) under our various usernames; all are freely accessible. If you need the raw images talk to us, we won't charge you much :). 

<blockquote class="twitter-tweet" data-theme="light"><p lang="en" dir="ltr">300+ GB of street-level imagery! Great job 💪<a href="https://twitter.com/stevdok?ref_src=twsrc%5Etfw">@stevdok</a> and team ✌. <a href="https://twitter.com/mapillary?ref_src=twsrc%5Etfw">@mapillary</a> unloading... <a href="https://twitter.com/hashtag/Ghana?src=hash&amp;ref_src=twsrc%5Etfw">#Ghana</a> <a href="https://twitter.com/hashtag/opendata?src=hash&amp;ref_src=twsrc%5Etfw">#opendata</a> <a href="https://twitter.com/hashtag/openstreetmap?src=hash&amp;ref_src=twsrc%5Etfw">#openstreetmap</a> <a href="https://twitter.com/hashtag/TravelGhana?src=hash&amp;ref_src=twsrc%5Etfw">#TravelGhana</a></p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/1143285717914783744?ref_src=twsrc%5Etfw">June 24, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>