---
title:  "OpenStreetMap Workshop in UMaT, Tarkwa"
date:   2015-10-21 18:11:12
tag:   
- openstreetmap 
- umat 
- tarkwa
image: /images/sev-osm-umat.jpg
---


By now you should all know am great fun of maps, simply put and am a GIS enthusiast.

OpenStreetMap is one great project and community that has nurtured that interest of mine. Am back to school after a long vacation.

On Friday October 16, 2015, Myself and some students most from the Geomatics Engineering Department of the University of Mines and Technology, Tarkwa were very glad to have Humanitarian OpenStreetMap Team (HOT) board member Severin Manard with us.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Glad to have @sev_hotosm in <a href="https://twitter.com/UMaT_edu_GH">@UMaT_edu_GH</a> for an intro to <a href="https://twitter.com/hashtag/OpenStreetMap?src=hash">#OpenStreetMap</a> workshop. cc <a href="https://twitter.com/OSMGhana">@OSMGhana</a> <a href="http://t.co/qD0sXb8Rj5">pic.twitter.com/qD0sXb8Rj5</a></p>&mdash; Enock Seth Nyamador (@Enock4seth) <a href="https://twitter.com/Enock4seth/status/654996481708044288">16 octobre 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Severin was travelling with other colleagues as part of ProjectEOF, but decided to make a stop in Tarkwa voluntarily to share his knowledge with us. Sev you coming here was more than the gold deposits we have :).

During the workshop Sev presented on what OpenStreetMap is all about as most students little or no idea about it; too bad I've not been able to make much impact before Sev's arrival. 

I helped the students install JRE and [JOSM](http://josm.openstreetmap.de/). Thanks Sev for providing internet access all from his pocket, what a generous volunteer. 

We also had a short field work on campus where students used their Android smartphones for GPX traces using OSM Tracker. They were very fascinated about how it works. They later exported the tracks and added them to JOSM. Where Sev guided them what and what next. Glad I could also offer a helping

After the short intro workshop on Friday, Sev's days of stay was very beneficial. Myself and some interested students had the opportunity to visit him at his guest house for interactions and further training. Thanks Sev for your time with us. We're very grateful. 

Also thanks to Mr. & Mrs. Mantey for hosting Sev. We're grateful for your support.

~~Well, if you're in or Accra and reading this, Sev will be at Mobile Web Ghana, Madina for a more intensive OSM and beyond workshop. You should join him.~~ Read about this workshop [here](http://www.codeforghana.org/2015/10/23/openstreetmap.html).

Merci Sev!
