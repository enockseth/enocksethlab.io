---
title:  "First OpenStreetMap Workshop, Ghana"
date:   2015-06-28 11:06:25
tag:   
- maps 
- openstreetmap 
- opendata
---

![OpenStreetMap workshop participants](/images/osm-participants.jpg)
Participants after workshop. *Photographer: Sylvester*

On June 27, 2015, I was in Madina, Accra for and [OpenStreetMap](http://osm.org) (OSM) workshop, this was the first time people in Ghana have gathered to edit and discuss OSM. This was in relation on the Accra Floods and putting Ghana on the map. Thanks to [CodeforGhana](http://www.codeforghana.org/) for providing the platform.

Maps are very important in our daily lives. Time spend on the phone asking for directions to a location can be eliminated if you know the location's address; say; *37 Golden Hills Avenue, Tarkwa.* That simply tells you house number **37** along the street **Golden Hills Avenue** in **Tarkwa**

It was sad only few of the participants who registered. Assuming you were one, next time you know that you can't attend an event don't just register, what if it was paid for?

We had mostly new mappers (students and developers), two people from the GIS industry and a Peace Corps volunteer.

During the workshop I did a short presentation on OpenStreetMap; Introducing what it is, tags, editors ([iD](http://learnosm.org/en/editing/id-editor/), [Potlatch](http://wiki.openstreetmap.org/wiki/Potlatch_2), [JOSM](http://wiki.openstreetmap.org/wiki/JOSM)), users, how we can use it and who uses OSM, Humanitarian OpenStreetMap Team, Impacts (Haiti earthquake, Ebola outbreak, Nepal Earthquake, etc) Missing Maps and OpenStreetMap Ghana. I also setup a simple task using [OSM Tasking Manager](http://tasks.hotosm.org/project/1100) which was centered around the Kwame Nkrumah Circle where [100s died in a petrol station explosion](https://en.wikipedia.org/wiki/2015_Accra_explosion) to introduce how mappers collaborate on projects.

![Enock Seth Nyamador](/images/enock-seth-nyamador.jpg)
Talking about "**Who uses OpenStreetMap?**"

Presentation can be found below:


<iframe src="//www.slideshare.net/slideshow/embed_code/key/zDenWTpuhIsYMK" width="700" height="525" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/Enock4seth/openstreetmap-workshop-accra" title="OpenStreetMap Workshop, Accra" target="_blank">OpenStreetMap Workshop, Accra</a> </strong> from <strong><a href="//www.slideshare.net/Enock4seth" target="_blank">Enock Seth Nyamador</a></strong> </div>

Glad to have Sylvester, a [GIS](https://en.wikipedia.org/wiki/GIS) professional and the planning officer for La-Nkwantanang-Madina Municipal Assembly, Mr. Kwesi Adarkwa. Before my presentation Sylvester talked about data from OpenStreetMap, how to get it and give back to the community. Not to forget he also told us about the his startup **MapOut Ghana**.

![Sylvester Adjanpong](/images/sylvester-adjapong.jpg)
Sylvester talking about importance of maps.

Even though it was a workshop to introduce and edit OSM. We used the time to also discuss how we can get people involved in OSM in Ghana especially students. Also came up was we shouldn't wait for something to happen before we start building a map. Not to forget we networked with each other.

If you're interested in contributing to OpenStreetMap, [beamapper.com](http://beamapper.com/) has it all summarized in 3 steps.

You can join OpenStreetMap Ghana mailing list [here](https://lists.openstreetmap.org/listinfo/talk-gh) to interact with other mappers or for any clarifications.

Also checkout [[Recap] OpenStreetMap Workshop](http://www.codeforghana.org/2015/06/30/openstreet.html) by CodeforGhana.

Need any help feel free to talk to myself or the mailing list provided above.

Thanks.
