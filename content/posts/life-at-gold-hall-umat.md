---
title:  "Life at Gold Hall, UMaT"
date:   2015-05-21 19:12:49
tag:   
- umat
- tarkwa
image: assets/images/umat-gold-hall.jpg
---

Last year I became a student of the **[University of Mines and Technology, Tarkwa](http://umat.edu.gh)**. As a resident student, I was assigned Gold Hall which is under the **Chamber of Mines Hall** as my hall of residence for Year 1.

As the name sounds one might be thinking, Wow! **Gold** Hall! at first mention. I'll like to use this opportunity to tell you just how it was during my stay.

![UMaT Gold Hall](/images/umat-gold-hall.jpg)Walking to Gold Hall.

Gold Refinery Hall is located at the highest point on the outskirts of Tarkwa and close to **GoldFields Ghana Limited** (*Tarkwa Mine*) . It stands tall among all buildings. My roommate Martin and other students who visited the **Ghana Manganese Company** in Nsuta once said they were able to see Gold Hall from that far, awesome!

Gold Hall was built to be used as a *Gold Refinery* during **Dr. Kwame Nkrumah**'s administration, but wasn't able to serve it's and was donated to then **Tarkwa School of Mines**

Gold Hall is say 7.5 km from the main campus of the University of Mines and Technology. 

At Gold Hall I was in room **GB9** close to us was the our sister room **GB8**.

My roommates were: *Combert, Martin and Adams*. We had temporal roommates after Adams decided to have a feel of campus :), they were *Sheriff* then *Adamu* (aka *Gbazaza*). I've learned a lot from all of them.

For all who have stayed at Gold Hall there is a story to tell but am not sure none have shared both the good and the bad.

During my stay in Gold Hall here are some stuffs I've taken note of:

* Gold Hall was where ECG once disconnect us for owing electricity bill. 
* Where our WiFi was installed few weeks to end of second semester. 
* Where we walk to that big reservoir to fetch water all because the pump isn't functioning or Baba Alahassan didn't turn it on early. 
* Gold Hall was where you had that *feeling* when everyone will be leaving to different halls and hostels in Level 200.
* When it's [Dumsor](http://enwp.org/Dumsor) night you see students going up and down the road leading to the **200 stairs** that connects Gold Hall to North Tarkwa Highway or you see **TA** and **TB** guys on the balcony. 
* Where **Mama Wagon** sells stuffs twice the price in town. 
* GoldHallers can vote can determine the winner of an election @ UMaT.
* Goldhallers are awesome, beautiful and handsome :).
* Where, those who walk to school are referred to as *Red bus*
* At Gold Hall we're united. Be it, Engineering Drawing Saturday or whatever. My very good friend, Adomako Ebenezer will say the **Voice of Gold Hall** :).

To me Gold Hall was really a great place and it shall continue to be. Most Goldhallers will say:
 
>When am given the chance to come back to UMaT level 100, I won't stay at Gold Hall.

For me it's different and it depends.

Long live UMaT, Long live Gold Hall.

Thanks for passing by.
